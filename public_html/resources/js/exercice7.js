/*
 PASSER D'UN OBJET JS A UN OBJET JSON

 var styloRouge = new Product("staedtler rouge", "2,50 €", "un beau stylo avec de l'encre rouge", "rouge", "13 cm", );

 function Product(name, price, description, color, size, image) {
 this.name = name;
 this.price = price;
 this.description = description;
 this.color = color;
 this.size = size;
 }

 console.log(styloRouge);

 var styloRougeJson = JSON.stringify(styloRouge);

 console.log(styloRougeJson);

 console.log(styloRougeJson.name);*/

/*
 *METHODE BIZARRE POUR CREER UN OBJET JS................
 *
 *var styloBleu = {"name": "staedtler bleu",
 *"price": "3 €",
 *"description": "un beau stylo avec de l'encre bleu",
 *"color": "bleu",
 *"size": "13 cm",
 *};
 *
 *console.log(styloBleu);
 *console.log(styloBleu.name);
 */

// PASSER D'UN OBJET JSON A UN OBJET JS

var styloBleu = "{\"name\": \"staedtler bleu\",\"price\": \"3 €\",\"description\": \"un beau stylo avec de l'encre bleu\",\"color\": \"bleu\",\"size\": \"13 cm\"}";

console.log(styloBleu);

var styloBleuJs = JSON.parse(styloBleu);

console.log(styloBleuJs);

console.log(styloBleuJs.name);

