"use strict";

var i = 0;

var leChatMachine = new Product("Le Chat Machine se met bien", "8,50 €", "La nouvelle lessive Le Chat Machine, plus ronronnant que jamais !", undefined, "70 cm", "../img/hqdefault.jpg");
var leChatBaby = new Product("Le Chat Machine qui fait du baby-foot", "12,50 €", "Le pilier de bar met des roustes", "gris", undefined, "../img/BF9Xb.jpg");

var tabProduct = [leChatMachine, leChatBaby];

function Product(name, price, description, color, size, image) {
    this.name = name;
    this.price = price;
    this.description = description;
    this.color = color;
    this.size = size;
    this.image = image;
}

function nextProduct() {
    i++;
    if (i > tabProduct.length - 1) {
        i = 0;
    }
    fiche(tabProduct[i]);
}

/* FONCTION MOINS ÉLÉGANTE ET MOINS GÉNÉRIQUE

 function nextProduct() {
 i++;
 if (i > 1) {
 i = 0;
 }
 if (i === 0) {
 fiche(leChatMachine);
 } else if (i === 1) {
 fiche(leChatBaby);
 }
 }*/

function fiche(object) {
    if (object instanceof Product) {
        document.getElementById('name').innerHTML = object.name;
        document.getElementById('price').innerHTML = object.price;
        document.getElementById('description').innerHTML = object.description;
        document.getElementById('color').innerHTML = object.color;
        document.getElementById('size').innerHTML = object.size;
        document.getElementById('imgChat').src = object.image;
    }
}

fiche(tabProduct[0]);