/*
 de 0 à 1 : bébé
 de 2 à 6 : jeune enfant
 de 7 à 11: enfant de l'age de raison
 de 12 à 17: adolescent
 de 17 à 30: jeune adulte
 de 31 à ???: vieux
 */

"use strict";

var ageUser;
var sexeUser;

function whichAge() {
    ageUser = document.getElementById("age").value;
    ageUser = parseInt(ageUser);
    alert(ageUser);
}

function whichSexe() {
    var radios = document.getElementsByName('f_sexe');
    var length = radios.length;
    for (var i = 0; i < length; i++)
    {
        if (radios[i].checked)
        {
            sexeUser = radios[i].value;
        }
    }
    alert(sexeUser);
}

function ageAndSexe(sentence) {
    switch (sexeUser) {
        case 'm':
            if (ageUser === 0 || ageUser === 1) {
                return "Vous êtes un petit bébé et ce n'est pas vous qui avez répondu à ce formulaire";
            } else if (ageUser > 1 && ageUser < 7) {
                return "Petit garçon, qu'est-ce que tu fouts sur les Internets !";
            }
            break;
        case 'f':
            break;
        default:
            break;
    }
}








// EXERCICE SUR LE | FOR EACH | FOR IN |

var tableau = ["Damien", "Lotfy", "Gael", "Marie", "Fanny", "Stephanie", "Kevin", "Mehdi", "Elisabeth", "Jérémy"];

console.log("Tableau 1 " + tableau);

tableau.forEach(function (nom) {
    console.log(nom);
});

var tableau2 = ["Damien", "Lotfy", "Gael", "Marie", "Fanny", "Stephanie", "Kevin", "Mehdi", "Elisabeth"];

console.log("Tableau 2 " + tableau2);

for (var i in tableau2) {
    console.log(tableau2[i]);
}
